create database dbapi;
use dbapi;

create table if not exists categorias(
	id bigint not null auto_increment,
    nome varchar(255) not null default '',
    descricao text not null default '',
    criadoEm datetime not null default now(),
    modificadoEm datetime not null default current_timestamp,
    primary key(id)
);

create table if not exists produtos(
	id bigint not null auto_increment,
    nome varchar(255) not null default '',
    descricao text not null default '',
    preco decimal(10,2) not null,
    idcategoria bigint not null default 0,
    criadoEm datetime not null default now(),
    modificadoEm datetime not null default current_timestamp,
    primary key(id),
    foreign key(idcategoria) references categorias(id)
);

insert into categorias(nome, descricao) values('Eletrodomésticos','Equipamentos eletrônicos para dentro de casa'), ('Produtos para revenda','equipamentos que podem ser passados à frente'),('Móveis','Coisas que são usadas no interior das casas');

insert into produtos(nome, descricao, idcategoria, preco) values ('Comôda','serve para guardar qualquer coisa em suas gavetas',3,500.99),('Liquidificador','serve para triturar os alimentos que forem lançados em seu interior',1,79.50),
	('Cadeira de balanço','os velhinhos usam para ficar sentados e ler sobre a vida nas revistas que eles dispõe',2,200.46);
    
-- select * from categorias;
-- select * from produtos;



select c.nome as nomeCategoria, p.id, p.nome, p.preco,
p.idcategoria, p.criadoem
from produtos as p
left join categorias as c on c.id = p.idcategoria
order by p.criadoem desc


