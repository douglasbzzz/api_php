<?php
    class Produtos{
        private $conn;
        private $tablename = "produtos";

        //propriedades da classe
        public $id;
        public $name;
        public $descricao;
        public $preco;
        public $idCategoria;
        public $criadoEm;

        //construtor

        public function __construct($db){
            $this->conn = $db;
        }

        function read(){
            $query = "select c.nome as nomeCategoria, p.id, p.nome, p.preco,
                      p.idcategoria, p.criadoem
                      from ".$this->tablename." as p
                      left join categorias as c on c.id = p.idcategoria
                      order by p.criadoem desc";
            
            $stmt =$this->conn->prepare($query);

            $stmt -> execute();

            return $stmt;
        }

        function create(){
            $query = "insert into ".$this->tablename." set nome=:nome, preco=:preco, descricao=:descricao,
                      idcategoria=:idcategoria, criadoEm=:criadoEm";
            
            $smtp = $this->conn->prepare($query);

            $this->nome=htmlspecialchars(strip_tags($this->nome));
            $this->preco=htmlspecialchars(strip_tags($this->preco));
            $this->descricao=htmlspecialchars(strip_tags($this->descricao));
            $this->idcategoria=htmlspecialchars(strip_tags($this->idCategoria));
            $this->criadoEm=htmlspecialchars(strip_tags($this->criadoEm));

            //bind values
            $stmt->bindParam(":nome",$this->nome);
            $stmt->bindParam(":preco",$this->preco);
            $stmt->bindParam(":descricao",$this->descricao);
            $stmt->bindParam(":idcategoria",$this->idCategoria);
            $stmt->bindParam(":criadoEm",$this->criadoEm);

            if($stmt->execute()){
                return true;
            }else{
                return false;
            }

        }
    }
?>