<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/database.php';
    include_once '../objects/product.php';

    $database = new Database();
    $db = $database->getConnection();

    $produto = new Produtos($db);

    $data = json_decode(file_get_contents("php://input"));

    if(
        !empty($data->nome) && 
        !empty($data->preco) &&
        !empty($data->descricao) &&
        !empty($data->idcategoria)
    ){
        $produto->nome = $data->nome;
        $produto->preco = $data->preco;
        $produto->descricao = $data->preco;
        $produto->idcategoria = $data->idcategoria;
        $produto->criadoEm = date('Y-m-d H:i:s');

        if($produto->create()){
            http_response_code(201);
            echo json_encode(array("message"=>"Produto Criado"));
        }else{
            http_response_code(503);
            echo json_encode(array("message"=>"Não foi possível criar o produto"));
        }
    }else{
        http_response_code(400);
        echo json_encode(array("message"=>"Não foi possível criar o produto, endereço incompleto"));
    }

?>